#include "sounddata.h"

DirectSound SoundData::directsound;
vector<Wave> SoundData::wave;
vector<SoundBuffer> SoundData::sb;
vector<int> SoundData::soundmanager;

SoundData::SoundData() {}
SoundData::~SoundData() {}

int SoundData::LoadSoundMem(TCHAR* filename)
{

	// メモリ確保
	wave.emplace_back();

	// データ保存
	wave.back().Load(filename);

	// 管理番号
	soundmanager.push_back(wave.size() - 1);

	// メモリ確保
	sb.emplace_back();

	// データ保存
	sb.back().Create(directsound.pDirectSound8, wave.back().WaveFormat, wave.back().WaveData, wave.back().DataSize);

	// 管理番号を返す
	return (soundmanager.size() - 1);
}

void SoundData::PlaySoundMem(int handle)
{
	// 再生
	sb[soundmanager[handle]].Play(false);
}

void SoundData::DeleteSoundMem(int handle)
{
	// メモリ解放
	wave[soundmanager[handle]].Delete();

	// 管理番号を新しい配列に適応させる
	for (int i = (handle + 1), n = soundmanager.size(); i < n; i++)
	{
		soundmanager[i]--;
	}

	// vectorの要素削除
	wave.erase(wave.begin() + handle);
	sb[soundmanager[handle]].Delete();
	sb.erase(sb.begin() + handle);
}

void SoundData::AllDeleteSoundMem()
{
	for (int i = 0, n = wave.size(); i < n; i++)
	{
		wave[i].Delete();	// テクスチャ全削除
	}
	for (int i = 0, n = sb.size();i < n;i++)
	{
		sb[i].Delete();
	}
	// vector全削除
	wave.clear();	// 要素数を０に
	wave.shrink_to_fit();	// 容量を要素数に合わせる
	sb.clear();
	sb.shrink_to_fit();
	soundmanager.clear();
	soundmanager.shrink_to_fit();
}