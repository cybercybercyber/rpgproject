#include <Windows.h>
#include <tchar.h>

#include "direct3d.h"
#include "texture.h"
#include "sprite.h"
#include "directsound.h"
#include "wave.h"
#include "soundbuffer.h"
#include "directxtext.h"
#include "GraphicData.h"
#include "SoundData.h"
#include "input.h"

// ウィンドウプロシージャ、ウィンドウに対するメッセージ処理を行う
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

	switch (msg) {
		// ウィンドウが破棄されたとき
	case WM_DESTROY:
		PostQuitMessage(0);	// WM_QUITメッセージをメッセージキューに送る
		return 0;
	}
	// デフォルトのメッセージ処理を行う
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

// WinMain関数（アプリケーションの開始関数）
// コンソールアプリケーションと違い、コンソールを開かない
int _stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	const TCHAR* WC_BASIC = _T("BASIC_WINDOW");
	// シンプルウィンドウクラス設定
	WNDCLASSEX wcex = { sizeof(WNDCLASSEX), CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,WndProc, 0,0,hInstance,
		(HICON)LoadImage(NULL,MAKEINTRESOURCE(IDI_APPLICATION),IMAGE_ICON,0,0,LR_DEFAULTSIZE | LR_SHARED),
		(HCURSOR)LoadImage(NULL,MAKEINTRESOURCE(IDC_ARROW),IMAGE_CURSOR,0,0,LR_DEFAULTSIZE | LR_SHARED),
		(HBRUSH)GetStockObject(WHITE_BRUSH), NULL, WC_BASIC , NULL };

	// シンプルウィンドウクラス作成
	if (!RegisterClassEx(&wcex))
		return false;

	// ウィンドウ幅、高さはディスプレイに依存する。普通は4:3
	const int WINDOW_WIDTH = 1920;
	const int WINDOW_HEIGHT = 1080;
	// ウィンドウの作成
	HWND hWnd = CreateWindowEx(0, WC_BASIC,
		_T("Application"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, WINDOW_WIDTH, WINDOW_HEIGHT, NULL, NULL, hInstance, NULL);

	////////////////////////////////////
	//	Direct3Dの初期化
	////////////////////////////////////
	direct3d.Create(hWnd, WINDOW_WIDTH, WINDOW_HEIGHT);
	InitRender();

	////////////////////////////////////
	//	フォント
	////////////////////////////////////
	directxtext.Create(direct3d.pDevice3D, 32);

	////////////////////////////////
	// DirectSoundデバイス作成
	////////////////////////////////
	directsound.Create(hWnd);

	// キー入力初期化
	Input::UpDate();

	// メッセージループ
	MSG msg = {};
	while (msg.message != WM_QUIT && Input::Get(VK_ESCAPE) == 0) {
		// アプリケーションに送られてくるメッセージをメッセージキューから取得する
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			DispatchMessage(&msg);	// アプリケーションの各ウィンドウプロシージャにメッセージを転送する
		}
		// メッセージ処理をしていないとき
		else {
			//（ここにDirectXの処理を書く）

			///////////////////////////////////////////////////////////////////////////////////////////////

			// キー入力更新
			Input::UpDate();

			// 描画開始
			if (SUCCEEDED(direct3d.pDevice3D->BeginScene()))
			{
				DWORD ClearColor = 0xff808080;	// 背景クリア色
				// 背景クリア
				direct3d.pDevice3D->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_STENCIL | D3DCLEAR_ZBUFFER, ClearColor, 1.0f, 0);

				// 描画終了
				direct3d.pDevice3D->EndScene();
			}
			// 描画反映
			direct3d.pDevice3D->Present(NULL, NULL, NULL, NULL);

			///////////////////////////////////////////////////////////////////////////////////////////////
		}
	}

	// 保険
	AllDeleteGraph();
	AllDeleteSoundMem();

	return 0;
}