#pragma once
#include <vector>
#include <deque>

#include "texture.h"
#include "sprite.h"
#include "directxtext.h"

using std::vector;

// define
#define InitRender		GraphicData::InitRender
#define LoadGraph		GraphicData::LoadGraph
#define direct3d		GraphicData::direct3d
#define directxtext		GraphicData::directxtext
#define DeleteGraph		GraphicData::DeleteGraph
#define DrawGraph		GraphicData::DrawGraph
#define AllDeleteGraph	GraphicData::AllDeleteGraph

class GraphicData {
private:
	static vector<Texture>	tex;
	static vector<Sprite>	sprite;
	static vector<int>		graphicmanager;
public:
	static Direct3D		direct3d;
	static DirectXText	directxtext;

	GraphicData();
	~GraphicData();

	// 透過処理
	static void InitRender();

	// 画像を読み込む関数
	static int LoadGraph(TCHAR* filename);

	// 読み込んだ画像データを削除する関数
	static void DeleteGraph(int handle);

	// 読み込んだ画像データを全て削除する関数
	static void AllDeleteGraph();

	// 画像を描写する関数
	static void DrawGraph(int x, int y, int handle);
};