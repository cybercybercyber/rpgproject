#pragma once
#include <vector>
#include <deque>

#include "wave.h"
#include "soundbuffer.h"

using namespace std;

// define
#define directsound			SoundData::directsound
#define LoadSoundMem		SoundData::LoadSoundMem
#define PlaySoundMem		SoundData::PlaySoundMem
#define DeleteSoundMem		SoundData::DeleteSoundMem
#define AllDeleteSoundMem	SoundData::AllDeleteSoundMem

class SoundData
{
private:
	static vector<Wave>	wave;			// サウンドデータ
	static vector<SoundBuffer> sb;		// セカンダリバッファ
	static vector<int> soundmanager;	// 管理番号

public:
	static DirectSound directsound;		// サウンドデバイス

	SoundData();
	~SoundData();

	// 音を読み込む
	static int LoadSoundMem(TCHAR* filename);
	// 音を再生する
	static void PlaySoundMem(int handle);
	// 指定した音を削除する
	static void DeleteSoundMem(int handle);
	// 読み込んだ音を全削除する
	static void AllDeleteSoundMem();
};