#include "graphicdata.h"

GraphicData::GraphicData() {}

GraphicData::~GraphicData() {}

Direct3D		GraphicData::direct3d;
DirectXText		GraphicData::directxtext;
vector<Texture> GraphicData::tex;
vector<Sprite>	GraphicData::sprite;
vector<int>		GraphicData::graphicmanager;

void GraphicData::InitRender()
{
	// アルファ・ブレンディングを行う
	direct3d.pDevice3D->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	// 透過処理を行う
	direct3d.pDevice3D->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
}

int GraphicData::LoadGraph(TCHAR* filename)
{
	// メモリ確保
	tex.emplace_back();
	// テクスチャ生成
	tex.back().Load(direct3d.pDevice3D, filename);
	// 管理番号確保
	graphicmanager.push_back(tex.size() - 1);
	// 画像情報構造体
	D3DXIMAGE_INFO info;
	// 画像情報取得
	D3DXGetImageInfoFromFile(filename, &info);
	// メモリ確保
	sprite.emplace_back();
	// スプライトの大きさ初期化
	sprite.back().SetWidth(info.Width, info.Height);

	// 管理番号を返す
	return (graphicmanager.size() - 1);
}

void GraphicData::DeleteGraph(int handle)
{
	// テクスチャ削除
	tex[graphicmanager[handle]].Delete();

	// 管理番号の適応させる
	for (int i = (handle + 1), n = graphicmanager.size(); i < n; i++)
	{
		graphicmanager[i]--;
	}

	// メモリ解放
	tex.erase(tex.begin() + graphicmanager[handle]);
	sprite.erase(sprite.begin() + graphicmanager[handle]);
}

void GraphicData::AllDeleteGraph()
{
	// テクスチャ全削除
	for (int i = 0, n = tex.size(); i < n; i++)
	{
		tex[i].Delete();
	}

	// vector全削除
	tex.clear();
	tex.shrink_to_fit();
	sprite.clear();
	sprite.shrink_to_fit();
	graphicmanager.clear();
	graphicmanager.shrink_to_fit();
}

void GraphicData::DrawGraph(int x, int y, int handle)
{
	sprite[graphicmanager[handle]].Draw(x, y, direct3d.pDevice3D, tex[graphicmanager[handle]].pTexture);
}