#include "input.h"

int Input::key[256];
SHORT Input::tmpKey[256];

Input::Input() {}
Input::~Input() {}

void Input::UpDate()
{
	for (int i = 0;i < 256;i++)
	{
		// [i]キーの入力状態を取得
		tmpKey[i] = GetKeyState(i);

		// 保存
		if (tmpKey[i] >= 0)
		{
			if (key[i] > 0)
			{
				key[i] = (-1);
			}
			else
			{
				key[i] = 0;
			}
		}
		else
		{
			key[i]++;
		}
	}
}

int Input::Get(int vkey)
{
	return key[vkey];
}