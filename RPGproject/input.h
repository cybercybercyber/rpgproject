#pragma once
#include "Windows.h"

class Input
{
private:
	static int key[256];	// キー入力状態保存
	static SHORT tmpKey[256];	// 現在の入力状態を表す

public:
	Input();
	~Input();

	static void UpDate();			// 入力状態更新
	static int Get(int vkey);	// キーの入力状態を得る
};